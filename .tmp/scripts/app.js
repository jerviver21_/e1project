class FormularioBusqueda extends React.Component {
  //Algunos de los props se convierte en el estado de este componente.
  constructor(props) {
    super();
    this.state = {
      usuario: props.usuario,
      incluirMiembro: props.incluirMiembro
    };
  }

  //Los handles permiten manejar eventos de submit, change, etc
  handleUsuario(ev) {
    this.setState({ usuario: ev.target.value });
  }

  handleIncluirMiembro(ev) {
    this.setState({ incluirMiembro: ev.target.checked });
  }

  handleSubmit(ev) {
    ev.preventDefault();
    this.props.onBuscar({
      usuario: this.state.usuario,
      incluirMiembro: this.state.incluirMiembro
    });
  }

  render() {
    return React.createElement("form", { className: "form-horizontal", onSubmit: this.handleSubmit.bind(this) }, React.createElement("div", { className: "form-group" }, React.createElement("div", { className: "col-lg-9" }, React.createElement("input", { className: "form-control", type: "text",
      value: this.state.usuario,
      onChange: this.handleUsuario.bind(this) })), React.createElement("div", { className: "col-lg-3" }, React.createElement("button", { className: "btn btn-default", type: "submit" }, "Buscar"))), React.createElement("div", { class: "form-group" }, React.createElement("div", { class: "col-lg-10" }, React.createElement("div", { class: "checkbox" }, React.createElement("label", null, React.createElement("input", { type: "checkbox",
      checked: this.state.incluirMiembro,
      onChange: this.handleIncluirMiembro.bind(this) }), " Incluir repositorios donde el usuario es miembro")))), React.createElement("hr", null));
  }
}

class ItemResultado extends React.Component {
  render() {
    var resultado = this.props.resultado;
    return React.createElement("li", { className: "list-group-item" }, React.createElement("div", { className: "row" }, React.createElement("div", { className: "col-lg-10" }, React.createElement("h4", null, React.createElement("a", { href: resultado.html_url, target: "blank" }, resultado.name), " ", resultado.private && React.createElement("span", { className: "resultado-privado" }, "Privado")), React.createElement("p", { className: "resultado-info" }, resultado.fork && React.createElement("span", { className: "resultado-fork" }, React.createElement("i", { className: "fa fa-code-fork" }), "Forkeado")), React.createElement("p", { className: "resultado-descripcion" }, resultado.descripcion), React.createElement("p", { className: "resultado-actualizado" }, "Actualizado ", moment(resultado.updated_at).fromNow())), React.createElement("div", { className: "col-lg-2 resultado-stats" }, React.createElement("span", { className: "resultado-stat" }, resultado.language, " I"), React.createElement("span", { className: "resultado-stat" }, React.createElement("i", { className: "fa fa-code-fork" }), resultado.forks_count), React.createElement("span", { className: "resultado-stat" }, React.createElement("i", { className: "fa fa-star" }), resultado.stargazers_count), React.createElement("span", { className: "resultado-stat" }, React.createElement("i", { className: "fa fa-eye" }), resultado.watchers_count))));
  }
}

class Resultados extends React.Component {
  render() {
    return React.createElement("ul", { className: "list-group" }, this.props.resultados.map(function (resultado) {
      return React.createElement(ItemResultado, { key: resultado.id, resultado: resultado });
    }.bind(this)));
  }
}

class App extends React.Component {
  //El estado permite darle dinamismo a la aplicación
  //se debe ver que cambia, los campos de entrada y los resultados de salida
  constructor() {
    super();
    this.state = {
      resultados: [],
      usuario: 'jerviver21',
      incluirMiembro: false
    };
  }

  //Es un hook, que se ejecuta al momento de iniciar.
  componentDidMount() {
    this.buscarResultados(this.state);
  }

  //Permite realizar el llamado a un API basados en un estado.
  buscarResultados(params) {
    var url = 'https://api.github.com/users/' + params.usuario + '/repos?sort=updated';
    if (params.incluirMiembro) {
      url += '&type=all';
    }

    fetch(url).then(function (response) {
      if (response.ok) {
        response.json().then(function (body) {
          this.setState({ resultados: body });
        }.bind(this));
      } else {
        this.setState({ resultados: [] });
      }
    }.bind(this));
  }

  //Funciona como un callback, este componente lo pasa como un props al formulario y cuando el form cambia de estado lo llama.
  cambiarCriterioBusqueda(estado) {
    this.setState(estado);
    this.buscarResultados(estado);
  }

  render() {
    return React.createElement("div", { className: "panel panel-default" }, React.createElement("div", { className: "panel-body" }, React.createElement(FormularioBusqueda, { usuario: this.state.usuario,
      incluirMiembro: this.state.incluirMiembro,
      onBuscar: this.cambiarCriterioBusqueda.bind(this) }), React.createElement(Resultados, { resultados: this.state.resultados })));
  }
}

ReactDOM.render(React.createElement(App, null), document.getElementById('content'));