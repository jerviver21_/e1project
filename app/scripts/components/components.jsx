class FormularioBusqueda extends React.Component{
  //Algunos de los props se convierte en el estado de este componente.
  constructor(props){
    super();
    this.state = {
      usuario : props.usuario,
      incluirMiembro : props.incluirMiembro
    }
  }

  //Los handles permiten manejar eventos de submit, change, etc
  handleUsuario(ev){
      this.setState({usuario : ev.target.value});
  }

  handleIncluirMiembro(ev){
      this.setState({incluirMiembro : ev.target.checked});
  }

  handleSubmit(ev){
      ev.preventDefault();
      this.props.onBuscar({
        usuario : this.state.usuario,
        incluirMiembro: this.state.incluirMiembro
      })
  }

  render(){
    return <form className="form-horizontal" onSubmit={this.handleSubmit.bind(this)}>
      <div className="form-group">
        <div className="col-lg-9">
          <input  className="form-control" type="text"
                      value={this.state.usuario}
                      onChange={this.handleUsuario.bind(this)}/>
        </div>
        <div  className="col-lg-3">
          <button className="btn btn-default" type="submit">Buscar</button>
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-10">
          <div class="checkbox">
            <label>
              <input type="checkbox"
                          checked={this.state.incluirMiembro}
                          onChange={this.handleIncluirMiembro.bind(this)}/>
              &nbsp;Incluir repositorios donde el usuario es miembro
            </label>
          </div>
        </div>
      </div>
      <hr/>
    </form>
    ;
  }
}

class ItemResultado extends React.Component{
  render(){
    var resultado = this.props.resultado;
    return <li className="list-group-item">
     <div className="row">
        <div className="col-lg-10">
        <h4>
            <a href={resultado.html_url} target="blank">
              {resultado.name}
            </a> {resultado.private && <span className="resultado-privado">Privado</span>}
          </h4>
          <p className="resultado-info">
              {
                resultado.fork && <span className="resultado-fork">
                  <i className="fa fa-code-fork"/>Forkeado
                </span>
              }
          </p>
          <p className="resultado-descripcion">{resultado.descripcion}</p>
          <p className="resultado-actualizado">Actualizado {moment(resultado.updated_at).fromNow()}</p>
        </div>

        <div className="col-lg-2 resultado-stats">
          <span className="resultado-stat">
            {resultado.language}&nbsp;I
          </span>
          <span className="resultado-stat">
            <i className="fa fa-code-fork"/>{resultado.forks_count}
          </span>
          <span className="resultado-stat">
            <i className="fa fa-star"/>{resultado.stargazers_count}
          </span>
          <span className="resultado-stat">
            <i className="fa fa-eye"/>{resultado.watchers_count}
          </span>
        </div>
      </div>
    </li>;
  }
}

class Resultados extends React.Component{
  render(){
    return <ul className="list-group">
      {this.props.resultados.map(function(resultado){
        return <ItemResultado key={resultado.id} resultado={resultado}/>;
      }.bind(this))}
    </ul>;
  }
}


class App  extends React.Component {
  //El estado permite darle dinamismo a la aplicación
  //se debe ver que cambia, los campos de entrada y los resultados de salida
  constructor(){
    super();
    this.state = {
      resultados : [],
      usuario : 'jerviver21',
      incluirMiembro: false
    }
  }

  //Es un hook, que se ejecuta al momento de iniciar.
  componentDidMount() {
    this.buscarResultados(this.state);
  }

  //Permite realizar el llamado a un API basados en un estado.
  buscarResultados(params){
    var url = 'https://api.github.com/users/'+params.usuario+'/repos?sort=updated';
    if(params.incluirMiembro){
        url += '&type=all';
    }

    fetch(url).then(function(response){
      if(response.ok){
        response.json().then(function(body){
            this.setState({resultados : body});
        }.bind(this));
      }else{
          this.setState({resultados : []});
      }
    }.bind(this));

  }

  //Funciona como un callback, este componente lo pasa como un props al formulario y cuando el form cambia de estado lo llama.
  cambiarCriterioBusqueda(estado){
    this.setState(estado);
    this.buscarResultados(estado);
  }

  render(){
    return <div className="panel panel-default">
        <div className="panel-body">
            <FormularioBusqueda  usuario={this.state.usuario}
                                                incluirMiembro={this.state.incluirMiembro}
                                                onBuscar={this.cambiarCriterioBusqueda.bind(this)}/>
            <Resultados resultados={this.state.resultados}/>
        </div>
    </div>;
  }
}

ReactDOM.render(<App />, document.getElementById('content'));
