var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

var babel        = require('gulp-babel');
var concat       = require('gulp-concat');
var eslint       = require('gulp-eslint');
var filter       = require('gulp-filter');
var notify       = require('gulp-notify');
var plumber      = require('gulp-plumber');
var react = require('gulp-react');

//Variables necesarias
//Plumber  vars, permiten el registro de errores de compilacion, en el  log del server
var onError = function(err) {
  notify.onError({
    title:    "Error",
    message:  "<%= error %>",
  })(err);
  this.emit('end');
};

var plumberOptions = {
  errorHandler: onError,
};
//****************


// Task for development
//Se copian todas las librerias de los vendors en el dir vendor, esto se puede igual concatenar con los otros scripts para que queden en app.js
gulp.task('vendorjs', function(){
	return gulp.src(['node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
					 'node_modules/jquery/dist/jquery.js',
					 'node_modules/react/dist/react.js',
					 'node_modules/react-dom/dist/react-dom.js',
					 'node_modules/moment/moment.js',
					 ''])
	.pipe(gulp.dest('.tmp/scripts/vendor'))
	.pipe(reload({stream: true}));
});

//Se compilan y se ponen en un archivo los componentes de react y funciones JS del proyecto
gulp.task('scriptjs', function(){
	return gulp.src(['app/scripts/components/components.jsx'])
		.pipe(sourcemaps.init())
		.pipe(react({harmony: false, es6module: true}))//Cambia sintaxis jsx a JS
		.pipe(babel())
		.pipe(concat('app.js'))
		.pipe(gulp.dest('.tmp/scripts'))
		.pipe(reload({stream: true}));
});


//Compilación y setup de los css
gulp.task('sass', function() {
	return gulp.src('app/styles/style.scss') // Leer un archivo
		.pipe(plumber(plumberOptions))//Muestra errores de compilación
    		.pipe(sourcemaps.init()) //Permite ver en el browser a que fuente de scss corresponde
		.pipe(sass()) // Compilar SASS
        	.pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false})) // Autoprfixer para que sea compatible en diferentes versiones de browsers
		.pipe(gulp.dest('.tmp/styles')) // Guardar archivo compilado en la version de desarrollo que permite ve3r cambios de inmediato
    		.pipe(sourcemaps.write())
		.pipe(reload({ stream: true })); // Recargar cambios al navegador
});

//Se copian las fuentes de font-awesome
gulp.task('fonts', function(){
	return gulp.src(['node_modules/font-awesome/fonts/*.*'])
	.pipe(gulp.dest('.tmp/fonts/font-awesome'))
	.pipe(reload({stream: true}));
});



gulp.task('watch', function() {
	gulp.watch('app/styles/style.scss', ['sass']); //Cualquier cambio en esa fuente, se compila y se recarga.
	gulp.watch('app/scripts/**/*.{js,jsx}', ['scriptjs']); //Cualquier cambio en cualquier html se recarga.
  	gulp.watch('app/*html').on('change', reload); //Cualquier cambio en cualquier html se recarga.
});

gulp.task('serve', ['sass', 'vendorjs', 'fonts', 'scriptjs'], function() {
	browserSync({
		server: {
			baseDir: ['.tmp', 'app'] //Directorio desde donde tomara las fuentes
		}
	});
    gulp.start('watch');
});


/*/ Task for production
gulp.task('sass:prod', function() {
	return gulp.src('app/styles/style.scss')
		.pipe(sass({ outputStyle: 'compressed'}).on('error', sass.logError)) // Compilar SASS minificado
		.pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false})) // Autoprefixer
		.pipe(gulp.dest('dist'));
});

gulp.task('bootstrajs:prod', function() {
	return gulp.src('node_modules/bootstrap-sass/assets/javascripts/bootstrap.js')
		.pipe(gulp.dest('dist/scripts/vendor'));
});

gulp.task('build', ['sass:prod', 'bootstrajs:prod']);
*/
